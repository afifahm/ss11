package com.apika.modul11.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewParent
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.apika.modul11.R
import com.apika.modul11.model.Photo
import com.bumptech.glide.Glide

class PhotoListAdapter (var photo: ArrayList<Photo>) {
    RecyclerView.Adapter<PhotoListAdapter.ViewHolder>(){
        fun updatePhoto(newPhotos: List<Photo>){
            photos.clear()
            photos.addAll(newPhotos)
            notifyDataSetChanged()
        }

        override fun onCreateViewHolder(parent: ViewGroup, p1: Int) = RecyclerView.ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_list, parent, false)
        )
        override fun getItemCount() = photos.size
        override fun onBindViewHolder(holder: ViewHolder, position: Int){
            holder.bind(photos[postion])
        }

        class ViewHolder(view: View) : RecyclerView.ViewHolder(view){
            fun bind(photos: Photo){
                itemView.tvTitle.text = photos.title
                itemView.setOnClickListener { v: View? ->
                    Toast.makeText(itemView.context, "Hello", Toast.LENGTH_LONG.show())
                }
                Glide.with(itemView.context).load(photos.thumbnail).into(itemView.imageView)
            }

        }
    }
}